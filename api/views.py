from django.core.mail import send_mail, send_mass_mail
from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import View
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

# Create your views here.


def home(request):
    return JsonResponse({'info': 'Laundry', 'name': 'Created By Sharayu'})


def showFirebaseJS(request):
    data = """
    // Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyC3Jfhl69BLK5oEZmXlk9vJtPe52ppapCA",
    authDomain: "djangoblog-302f1.firebaseapp.com",

    projectId: "djangoblog-302f1",
    storageBucket: "djangoblog-302f1.appspot.com",
    messagingSenderId: "1022004839184",
    appId: "1:1022004839184:web:f4da17dab14c85657ddb95",
    measurementId: "G-C6FGHP8LMH"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const notification = JSON.parse(payload);
    const notificationOption = {
        body: notification.body,
        icon: notification.icon
    }
    return self.registration.showNotification(payload.notification.title, notificationOption);
});
    """
    return HttpResponse(data, content_type='application/javascript')


class EmailBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None
