from api.comment.models import comment
from django.urls import path, re_path, include


from api.comment.views import comment_detail, comment_list
from rest_framework.urlpatterns import format_suffix_patterns
# from django.contrib.auth import views as auth_views


urlpatterns = [

    # Comment APi
    path('', comment_list),
    path('<int:shop>', comment_detail),

]

urlpatterns = format_suffix_patterns(urlpatterns)


# router = routers.DefaultRouter()
# router.register(r'', snippet_list)
