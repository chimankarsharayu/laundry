from rest_framework import serializers
from api.comment.models import comment


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = comment
        fields = ['id', 'shop', 'user', 'body']
