from django.db import models

# Create your models here.
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.contrib.auth import get_user_model
from api.shop.models import shop

# Create your models here.


class comment(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    shop = models.ForeignKey(
        shop,  related_name="comments",  null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(
        get_user_model(), db_column='user', null=True, on_delete=models.CASCADE)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'comment'

    def __str__(self):
        return self.body
