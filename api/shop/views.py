from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import viewsets
from api.shop.models import shop
from api.shop.serializers import ShopSerializer, oneShopSerializer,SearchShopSerializer
# from api.shop.models import fcm_ids
# from api.shop import FCMManager
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q


@api_view(['GET', 'POST'])
def shop_list(request):
    """
    List all shops
    """
    if request.method == 'GET':
        shops = shop.objects.all().order_by("-id")
        serializer = ShopSerializer(shops, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ShopSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def shop_detail(request, pk):
    """
    Retrieve, update or delete a shop.
    """
    try:
        snippet = shop.objects.get(author=pk)
    except shop.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = oneShopSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = oneShopSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# Search shops
@api_view(['GET'])
def searchShop(request, pk):
    """
    Retrieve, update or delete a blogs.
    """
    try:
        snippet = shop.objects.filter(Q(shop_name__icontains=pk) |Q(shop_address__icontains=pk))
    except shop.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SearchShopSerializer(snippet,many=True)
        return Response(serializer.data)
