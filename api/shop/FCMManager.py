import firebase_admin
from firebase_admin import credentials, messaging

cred = credentials.Certificate(
    "/media/sharayu/Drive1/Django_project/DjangoBlog/api/blog/djangoblog-302f1-firebase-adminsdk-4tw37-3839111d29.json")
firebase_admin.initialize_app(cred)


def sendPush(title, msg,  registration_token, dataObject):
    message = messaging.MulticastMessage(
        notification=messaging.Notification(
            title=title, body=msg,),
        data=dataObject,
        tokens=registration_token
    )

    response = messaging.send_multicast(message)
    print("successsfully sent message")
