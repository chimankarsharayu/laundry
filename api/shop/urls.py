from rest_framework import routers
from django.urls import path, include

from api.shop.views import shop_detail, shop_list,searchShop
from rest_framework.urlpatterns import format_suffix_patterns


# router = routers.DefaultRouter()
# router.register(r'', snippet_list)

urlpatterns = [
    path('', shop_list),
    path('<int:pk>', shop_detail),
    path('search/<str:pk>', searchShop),


]
urlpatterns = format_suffix_patterns(urlpatterns)
