from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.contrib.auth import get_user_model
from cloudinary.models import CloudinaryField


# Create your models here.


class shop(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    shop_name = models.CharField(max_length=200)
    author = models.ForeignKey(
        get_user_model(), null=True, on_delete=models.CASCADE)
    # photo = CloudinaryField('blog')
    shop_address = models.CharField(max_length=500, null=True)
    t_from = models.CharField(max_length=500, null=True)
    t_to = models.CharField(max_length=500, null=True)

    # Dry Clean Rates
    dd_shirt = models.IntegerField(null=True, default=0)
    dd_shirtsilk = models.IntegerField(null=True, default=0)
    dd_ladiestop = models.IntegerField(null=True, default=0)
    dd_trouser = models.IntegerField(null=True, default=0)
    de_payjama = models.IntegerField(null=True, default=0)
    de_kurta = models.IntegerField(null=True, default=0)
    de_saree = models.IntegerField(null=True, default=0)
    de_lehenga = models.IntegerField(null=True, default=0)
    dw_sweatshirt = models.IntegerField(null=True, default=0)
    dw_sweater = models.IntegerField(null=True, default=0)
    dw_shawl = models.IntegerField(null=True, default=0)
    dw_blanket = models.IntegerField(null=True, default=0)
    dm_bedsheet = models.IntegerField(null=True, default=0)
    dm_bedcover = models.IntegerField(null=True, default=0)
    dm_cushion = models.IntegerField(null=True, default=0)
    dm_curtaindoor = models.IntegerField(null=True, default=0)

    # Organic dry clean rates
    od_shirt = models.IntegerField(null=True, default=0)
    od_shirtsilk = models.IntegerField(null=True, default=0)
    od_ladiestop = models.IntegerField(null=True, default=0)
    od_trouser = models.IntegerField(null=True, default=0)
    oe_payjama = models.IntegerField(null=True, default=0)
    oe_kurta = models.IntegerField(null=True, default=0)
    oe_saree = models.IntegerField(null=True, default=0)
    oe_lehenga = models.IntegerField(null=True, default=0)
    ow_sweatshirt = models.IntegerField(null=True, default=0)
    ow_sweater = models.IntegerField(null=True, default=0)
    ow_shawl = models.IntegerField(null=True, default=0)
    ow_blanket = models.IntegerField(null=True, default=0)

    # Jacket Spa Rates
    j_leatherjacket = models.IntegerField(null=True, default=0)
    j_rexineleatherjacket = models.IntegerField(null=True, default=0)
    j_exoticleatherjacket = models.IntegerField(null=True, default=0)

    # Washing Services
    wi_normal = models.IntegerField(null=True, default=0)
    wi_organic = models.IntegerField(null=True, default=0)
    wi_premium = models.IntegerField(null=True, default=0)

    wf_normal = models.IntegerField(null=True, default=0)
    wf_organic = models.IntegerField(null=True, default=0)
    wf_premium = models.IntegerField(null=True, default=0)

    s_normal = models.IntegerField(null=True, default=0)
    s_organic = models.IntegerField(null=True, default=0)
    s_premium = models.IntegerField(null=True, default=0)

    # SHoe and bags services
    s_drycleaning = models.IntegerField(null=True, default=0)
    s_recoloring = models.IntegerField(null=True, default=0)
    s_stiching = models.IntegerField(null=True, default=0)
    s_lace = models.IntegerField(null=True, default=0)

    b_polishing = models.IntegerField(null=True, default=0)
    b_conditioning = models.IntegerField(null=True, default=0)
    b_recoloring = models.IntegerField(null=True, default=0)
    b_stiching = models.IntegerField(null=True, default=0)

    #
    # body = RichTextUploadingField(blank=True, null=True)
    # url = models.CharField(max_length=200, null=True)
    # likes = models.ManyToManyField(
    #     get_user_model(), related_name="blog_posts", null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # def total_likes(self):
    #     return self.likes.count()

    class Meta:
        db_table = 'shop'

    def __str__(self):
        return self.shop_name

    def get_absolute_url(self):
        # return reverse("article-detail", args=(str(self.id)))
        return reverse("postpage")
