from django import forms
from django.db.models import fields
from django.http import request
from api.shop.models import shop
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingFormField
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from cloudinary.forms import CloudinaryFileField


class shopform(forms.ModelForm):
    # body = forms.CharField(widget=CKEditorUploadingWidget())
    # body = RichTextUploadingFormField()

    class Meta:
        model = shop
        fields = '_all__'
        # widgets = {
        #     'body': CKEditorUploadingWidget(),
        # }


class AddForm(forms.ModelForm):
    # photo = CloudinaryFileField(
    #     options={
    #         'crop': 'thumb',
    #         'width': 200,
    #         'height': 200,
    #         'folder': 'blog'
    #     }
    # )

    class Meta:
        model = shop
        # fields = ('title', 'body', 'decription', 'author', 'photo')
        fields = '_all__'

        # widgets = {
        #     'title': forms.TextInput(attrs={'class': 'form-control'}),
        #     'body': forms.Textarea(attrs={'class': 'form-control'}),
        #     # 'photo': forms.FileInput(attrs={'class': 'form-control'}),
        #     'author': forms.Select(attrs={'class': 'form-control', }),
        #     'decription': forms.TextInput(attrs={'class': 'form-control'}),
        #     # 'author': forms.Select(attrs={'class': 'form-control'}),

        # }


class EditForm(forms.ModelForm):
    class Meta:
        model = shop
        # fields = ('title', 'body', 'decription', 'photo')
        fields = '_all__'

        # widgets = {
        #     'title': forms.TextInput(attrs={'class': 'form-control'}),
        #     'body': forms.Textarea(attrs={'class': 'form-control'}),
        #     # 'photo': forms.FileInput(),
        #     # 'author': forms.Select(attrs={'class': 'form-control'}),
        #     'decription': forms.TextInput(attrs={'class': 'form-control'}),
        #     # 'author': forms.Select(attrs={'class': 'form-control'}),

        # }
