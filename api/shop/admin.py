from django.contrib import admin
from api.shop.models import shop

# Register your models here.


class ShopModelAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(ShopModelAdmin, self).get_queryset(request)
        return qs.filter(author=request.user)


admin.site.register(shop, ShopModelAdmin)
