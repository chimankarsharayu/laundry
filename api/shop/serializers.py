from rest_framework import serializers
from api.shop.models import shop


class ShopSerializer(serializers.ModelSerializer):
    # author = serializers.SerializerMethodField()

    # def get_author(self, obj):
    #     return obj.author.id, obj.author.name,

    class Meta:
        model = shop
        fields = '__all__'


class SearchShopSerializer(serializers.ModelSerializer):
    # author = serializers.SerializerMethodField()

    # def get_author(self, obj):
    #     return obj.author.id, obj.author.name,

    class Meta:
        model = shop
        fields = '__all__'


class oneShopSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        return obj.author.id, obj.author.name,

    class Meta:
        model = shop
        # fields = ['id', 'title', 'body',
        #           'decription', 'author', 'photo', 'url']
        fields = '__all__'
