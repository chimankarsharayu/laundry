from django.db import migrations
from api.user.models import CustomUser


class Migration(migrations.Migration):
    def seed_data(apps, schema_editor):
        user = CustomUser(name="Akansha", email="akankshadudhkaware@gmail.com",
                          is_staff=True, is_superuser=True, gender="Female", phone="8709239590")

        user.set_password("12345")
        user.save()

    dependencies = [

    ]

    operations = [
        migrations.RunPython(seed_data),
    ]
