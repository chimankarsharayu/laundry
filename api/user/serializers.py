from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import authentication_classes, permission_classes
from django.contrib.auth import get_user_model
from .models import CustomUser
from rest_framework.serializers import ModelSerializer, CharField


class UserSerializer(serializers.HyperlinkedModelSerializer):

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)

        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)

        instance.save()
        return instance

    class Meta:

        model = CustomUser
        extra_kwargs = {'password': {'write_only': True}}
        fields = ['name', 'email', 'password', 'phone', 'gender',
                  'isAdmin', 'id'
                  ]

class oneUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        # fields = ['id', 'title', 'body',
        #           'decription', 'author', 'photo', 'url']
        fields = '__all__'



class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = CustomUser
        fields = ('old_password', 'password')

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError(
                {"password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):

        instance.set_password(validated_data['password'])
        instance.save()

        return instance
