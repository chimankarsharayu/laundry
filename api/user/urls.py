from rest_framework import routers
from django.urls import path, include
from django.views.generic import TemplateView
from .views import UserViewSet, signin, signout, ChangePasswordView, user_detail
from django.views.decorators.csrf import csrf_exempt


router = routers.DefaultRouter()
router.register(r'', UserViewSet)

urlpatterns = [
    path('login/', signin, name='signin'),
    path('logout/<int:id>', signout, name='signout'),
    path('get/<str:value>', user_detail),
    path('', include(router.urls)),
    path('change_password/<int:pk>/', csrf_exempt(ChangePasswordView.as_view()),
         name='auth_change_password'),

]
