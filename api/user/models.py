from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.core import mail

# Create your models here.


class CustomUser(AbstractUser):
    name = models.CharField(max_length=50, default='Anonymous')
    email = models.CharField(max_length=250, unique=True)

    username = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    phone = models.CharField(max_length=50, blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    isAdmin = models.CharField(
        max_length=10, blank=True, null=True, default='0')
    session_token = models.CharField(max_length=10, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):

    message = Mail(
        from_email='infozinterns@gmail.com',
        to_emails=reset_password_token.user.email,
        subject='Test By sharayu',
        html_content="http://127.0.0.1:8000/api/password_reset/confirm/?token={}".format(
            reset_password_token.key))
    try:
        sg = SendGridAPIClient(
            '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')
        response = sg.send(message)
        print("stattus Code ", response.status_code)
        print("body", response.body)
        print("headers", response.headers)

    except Exception as e:
        print(message)

    # email_plaintext_message = "{}?token={}".format(
    #     reverse('password_reset:reset-password-request'), reset_password_token.key)

    # send_mail(
    #     # title:
    #     "Password Reset for {title}".format(title="Some website title"),
    #     # message:
    #     email_plaintext_message,
    #     # from:
    #     'infozinterns@gmail.com',
    #     # to:
    #     [reset_password_token.user.email]
    # )
