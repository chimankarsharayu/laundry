from rest_framework import viewsets, generics, mixins, permissions, parsers,status
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import UserSerializer, oneUserSerializer
from .models import CustomUser
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout
import random
import re
from api.user.serializers import ChangePasswordSerializer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.decorators import parser_classes
from rest_framework.decorators import api_view
from rest_framework import status




# Create your views here.


def generate_session_token(length=10):
    return ''.join(random.SystemRandom().choice([chr(i) for i in range(97, 123)] + [str(i) for i in range(10)]) for _ in range(length))


@csrf_exempt
def signin(request):
    if not request.method == 'POST':
        return JsonResponse({'error': 'Send a post request with valid paramenter only'},status=status.HTTP_400_BAD_REQUEST)

    # print(request.POST.get('email', None))  - if you will not get email, None will be printed
    print(request.body)
    username = request.POST['email']
    password = request.POST['password']

    print(username)

    print(password)

# validation part
    if not re.match("^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$", username):
        return JsonResponse({'error': 'Enter a valid email'},status=status.HTTP_400_BAD_REQUEST)

    if (len(password) < 3):
        return JsonResponse({'error': 'Password needs to be at least of 3 char'},status=status.HTTP_400_BAD_REQUEST)

    UserModel = get_user_model()

    try:
        user = UserModel.objects.get(email=username)

        if user.check_password(password):
            usr_dict = UserModel.objects.filter(
                email=username).values().first()
            usr_dict.pop('password')

            # if user.session_token != "0":
            #     user.session_token = "0"
            #     user.save()
            #     return JsonResponse({'error': "Previous session exists!"},status=status.HTTP_400_BAD_REQUEST)

            token = generate_session_token()
            user.session_token = token
            user.save()
            login(request, user)
            mydata ={'token': token, 'user': usr_dict}
            return JsonResponse(mydata,status=200)
            # return Response({'something': 'my custom JSON'})
        else:
            return JsonResponse({'error': 'Invalid password'},status=401)

    except UserModel.DoesNotExist:
        return JsonResponse({'error': 'Invalid Email'},status=401)


def signout(request, id):
    logout(request)

    UserModel = get_user_model()

    try:
        user = UserModel.objects.get(pk=id)
        user.session_token = "0"
        user.save()

    except UserModel.DoesNotExist:
        return JsonResponse({'error': 'Invalid user ID'})

    return JsonResponse({'success': 'Logout success'})


class ChangePasswordView(generics.UpdateAPIView):
    # permission_classes = (IsAuthenticated)
    queryset = CustomUser.objects.all()
    serializer_class = ChangePasswordSerializer


class UserViewSet(viewsets.ModelViewSet):
    permission_classes_by_action = {'create': [AllowAny]}

    queryset = CustomUser.objects.all().order_by('id')
    serializer_class = UserSerializer

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]

        except KeyError:
            return [permission() for permission in self.permission_classes]


@api_view(['GET'])
def user_detail(request, value):
    """
    Retrieve, update or delete a blogs.
    """
    try:
        snippet = CustomUser.objects.get(email=value)
    except CustomUser.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = oneUserSerializer(snippet)
        return Response(serializer.data)

