from django.db import models

# Create your models here.
from django.db import models
# from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.contrib.auth import get_user_model
from api.shop.models import shop
from api.order.models import order

# Create your models here.


class history(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    shop = models.ForeignKey(
        shop,  null=True, on_delete=models.CASCADE)
    author = models.ForeignKey(
        get_user_model(), db_column='author', null=True, on_delete=models.CASCADE)
    # order_id = models.ForeignKey(
    #     order, db_column='order_id', null=True, on_delete=models.CASCADE)
    order_id = models.IntegerField(null=True)
    payment = models.IntegerField(null=True, default=0)
    paymentStatus = models.IntegerField(null=True, default=0)
    orderStatus = models.IntegerField(null=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'history'

    def __str__(self):
        return self.shop
