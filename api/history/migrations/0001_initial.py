# Generated by Django 3.2.3 on 2022-02-02 13:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0003_auto_20211116_1838'),
    ]

    operations = [
        migrations.CreateModel(
            name='history',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('payment', models.IntegerField(default=0, null=True)),
                ('paymentStatus', models.IntegerField(default=0, null=True)),
                ('orderStatus', models.IntegerField(default=0, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(db_column='author', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('shop', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.shop')),
            ],
            options={
                'db_table': 'history',
            },
        ),
    ]
