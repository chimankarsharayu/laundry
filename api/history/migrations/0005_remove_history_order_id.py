# Generated by Django 3.2.3 on 2022-02-02 18:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0004_alter_history_order_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='history',
            name='order_id',
        ),
    ]
