from rest_framework import serializers
from api.history.models import history


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = history
        fields = '__all__'


class UserHistorySerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    shop = serializers.SerializerMethodField()

    def get_author(self, obj):
        return obj.author.id, obj.author.name
    def get_shop(self, obj):
        return obj.shop.id, obj.shop.shop_name
    class Meta:
        model = history
        fields = '__all__'

class oneHistorySerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        return obj.author.id, obj.author.name,

    class Meta:
        model = history
        fields = '__all__'


class HistoryByOrderIDSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        return obj.author.id, obj.author.name,

    class Meta:
        model = history
        fields = '__all__'
