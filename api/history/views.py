from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import viewsets
from api.history.models import history
from api.history.serializers import HistorySerializer, oneHistorySerializer,UserHistorySerializer,HistoryByOrderIDSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt


@api_view(['GET', 'POST'])
def history_list(request):
    """
    List all Orders
    """
    if request.method == 'GET':
        historys = history.objects.all()
        serializer = HistorySerializer(historys, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = HistorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def history_detail(request, pk):
    """
    Retrieve, update or delete a history.
    """
    try:
        snippet = history.objects.get(pk=pk)
    except history.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = oneHistorySerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = oneHistorySerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def historyUser_detail(request, pk):
    """
    Retrieve, update or delete a history.
    """
    try:

        snippet = history.objects.filter(author=pk)
    except history.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':

        serializer = UserHistorySerializer(snippet,many=True)
        return Response(serializer.data)

@api_view(['GET'])
def historyByOrderIdU_detail(request, pk):
    """
    Retrieve, update or delete a blogs.
    """
    try:

        snippet = history.objects.filter(order_id=pk)
    except history.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':

        serializer = HistoryByOrderIDSerializer(snippet,many=True)
        return Response(serializer.data)







