from rest_framework import routers
from django.urls import path, include

from api.history.views import history_list, history_detail,historyUser_detail,historyByOrderIdU_detail
from rest_framework.urlpatterns import format_suffix_patterns


# router = routers.DefaultRouter()
# router.register(r'', snippet_list)

urlpatterns = [
    path('', history_list),
    path('<int:pk>', history_detail),
    path('getUserHistory/<int:pk>', historyUser_detail),
    path('getHistoryByOrderID/<int:pk>', historyByOrderIdU_detail),
]
urlpatterns = format_suffix_patterns(urlpatterns)
