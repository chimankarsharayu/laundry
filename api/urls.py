from django.urls import path, include
from rest_framework.authtoken import views
from api.views import home


urlpatterns = [
    path('', home, name='api.home'),

    path('user/', include('api.user.urls')),
    # path('fcm/', include('api.fcm.urls')),
    path('shop/', include('api.shop.urls')),
    path('comment/', include('api.comment.urls')),
    path('order/', include('api.order.urls')),
    path('history/', include('api.history.urls')),
    path('fcm/', include('api.fcm.urls')),

    path('api-token-auth/', views.obtain_auth_token, name="api-token_auth"),

]
