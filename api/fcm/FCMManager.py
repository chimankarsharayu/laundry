import firebase_admin
from firebase_admin import credentials, messaging

cred = credentials.Certificate(
    "/home/Laundryapp12/Laundary/api/fcm/laundry-48a6a-firebase-adminsdk-wp0z0-68cf4ec157.json")
firebase_admin.initialize_app(cred)


def sendPush(title, msg,  registration_token):
    message = messaging.Message(
        notification=messaging.Notification(
            title=title, body=msg,),
        # data=dataObject,
        token=registration_token
    )

    response = messaging.send(message)
    print("successsfully sent message")