from rest_framework import serializers
from api.fcm.models import fcm


class FCMSerializer(serializers.ModelSerializer):
    class Meta:
        model = fcm
        fields = '__all__'

class oneFCMSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    def get_author(self, obj):
        return obj.author.id, obj.author.name,

    class Meta:
        model = fcm
        fields = '__all__'