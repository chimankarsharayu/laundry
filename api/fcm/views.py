from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import viewsets
from api.fcm.models import fcm
from api.fcm.serializers import FCMSerializer,oneFCMSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
import json
from api.fcm import FCMManager

@api_view(['GET', 'POST'])
def fcm_list(request):
    """
    List all Orders
    """
    if request.method == 'GET':
        fcms = fcm.objects.all()
        serializer = FCMSerializer(fcms, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = FCMSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['POST'])
def get_notify(request):
    """
    List all notify
    """
    if request.method == 'POST':
        serializer = FCMSerializer(data=request.data)


        id = request.POST.get('author')
        title = request.POST.get('title')
        body = request.POST.get('body')

        tokens = fcm.objects.get(author=id)
        print(tokens.fcmid)
        # tokenlist =[]
        # for token in tokens:
        #     tokenlist.append(token.fcmid)
        #     print(token.fcmid)
        fcm_id=tokens.fcmid

        FCMManager.sendPush(title, body, fcm_id)

        data = {
            "id":id,
            "title":title,
            "body":body,
            "fcm":tokens.fcmid,
            "status":200
            }
        print()
        return Response(data)


@api_view(['GET', 'PUT', 'DELETE'])
def fcm_detail(request, pk):
    """
    Retrieve, update or delete a fcm.
    """
    try:
        snippet = fcm.objects.get(author=pk)
    except fcm.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = oneFCMSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = oneFCMSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)