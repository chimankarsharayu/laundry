from django.db import models

# Create your models here.
from django.db import models
# from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.contrib.auth import get_user_model


# Create your models here.


class fcm(models.Model):
    id = models.AutoField(primary_key=True, null=False)

    author = models.ForeignKey(
        get_user_model(), db_column='author', null=True, on_delete=models.CASCADE,unique=True)
    fcmid = models.CharField(max_length=200, null=True, default=None,unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'fcm'

    def __str__(self):
        return self.id
