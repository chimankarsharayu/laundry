# Generated by Django 3.2.3 on 2022-02-12 13:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fcm', '0007_alter_fcm_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fcm',
            name='author',
            field=models.ForeignKey(db_column='author', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, unique=True),
        ),
    ]
