# Generated by Django 3.2.3 on 2022-02-12 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcm', '0005_alter_fcm_fcmid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fcm',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
