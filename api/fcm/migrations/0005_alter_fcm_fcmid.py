# Generated by Django 3.2.3 on 2022-02-12 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fcm', '0004_auto_20220212_0629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fcm',
            name='fcmid',
            field=models.CharField(default=None, max_length=200, null=True, unique=True),
        ),
    ]
