from rest_framework import routers
from django.urls import path, include

from api.fcm.views import fcm_list, fcm_detail,get_notify
from rest_framework.urlpatterns import format_suffix_patterns


# router = routers.DefaultRouter()
# router.register(r'', snippet_list)

urlpatterns = [
    path('', fcm_list),
    path('<int:pk>', fcm_detail),
    path('getnotify', get_notify),

]
urlpatterns = format_suffix_patterns(urlpatterns)
