# Generated by Django 3.2.3 on 2022-02-13 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_order_paymentmode'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='complaint',
            field=models.CharField(default=None, max_length=2500, null=True),
        ),
    ]
