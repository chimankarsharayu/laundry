from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework import viewsets
from api.order.models import order
from api.order.serializers import OrderSerializer, oneOrderSerializer,UserOrderSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt


@api_view(['GET', 'POST'])
def order_list(request):
    """
    List all Orders
    """
    if request.method == 'GET':
        orders = order.objects.all()
        serializer = OrderSerializer(orders, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def order_detail(request, pk):
    """
    Retrieve, update or delete a blogs.
    """
    try:
        snippet = order.objects.get(user=pk)
    except order.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = oneOrderSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = oneOrderSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def orderUser_detail(request, pk):
    """
    Retrieve, update or delete a blogs.
    """
    try:

        snippet = order.objects.filter(shop_name=pk)
    except order.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':

        serializer = UserOrderSerializer(snippet,many=True)
        return Response(serializer.data)

