from rest_framework import routers
from django.urls import path, include

from api.order.views import order_list, order_detail,orderUser_detail
from rest_framework.urlpatterns import format_suffix_patterns


# router = routers.DefaultRouter()
# router.register(r'', snippet_list)

urlpatterns = [
    path('', order_list),
    path('<int:pk>', order_detail),
    path('getUserOrder/<int:pk>', orderUser_detail),

]
urlpatterns = format_suffix_patterns(urlpatterns)
