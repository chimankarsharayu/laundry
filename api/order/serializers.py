from rest_framework import serializers
from api.order.models import order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = order
        fields = '__all__'

class UserOrderSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    def get_user(self, obj):
        return obj.user.id, obj.user.name,obj.user.phone
    class Meta:
        model = order
        fields = '__all__'


class oneOrderSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()

    def get_user(self, obj):
        return obj.user.id, obj.user.name,

    class Meta:
        model = order
        fields = '__all__'
