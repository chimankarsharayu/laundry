from django.db import models
from django.contrib.auth import get_user_model
from api.shop.models import shop


class order(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    shop_name = models.ForeignKey(
        shop, db_column='shop_name',  null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(
        get_user_model(), null=True, on_delete=models.CASCADE)
    pickup_address = models.CharField(max_length=1000, null=True)
    day_pickup = models.CharField(max_length=500,null = True)
    time_slot = models.CharField(max_length=100,null = True)
    track_order = models.CharField(max_length=100, null=True)
    payment = models.IntegerField(null=True, default=0)
    quantity = models.IntegerField(null=True, default=0)
    coupon = models.CharField(max_length=100,null=True,default=None)
    complaint = models.CharField(max_length=2500,null=True,default=None)
    paymentMode = models.CharField(max_length=100,null=True,default=None)
    paymentStatus = models.IntegerField(null=True, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'order'

    def __str__(self):
        return self.shop_name
